/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swscw1;

import java.io.IOException;
import java.io.PrintWriter;
import org.apache.jena.rdf.model.Model ;
import org.apache.jena.rdf.model.ModelFactory ;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;

public class Swscw1
{
    public static void main(String... argv) throws IOException {
        Model m = ModelFactory.createDefaultModel() ;
        // read into the model.
        String current = new java.io.File( "." ).getCanonicalPath() + "/src/input/dataSWS1.ttl";
        m.read(current) ;
        StmtIterator it = m.listStatements();
        PrintWriter writer = null;
        try {
            writer = new PrintWriter("src/input/ttlAsTxtFile.txt", "UTF-8");
        } catch(Exception e){
        System.out.println("Failed to initialize writer.");
        }
                
        
        while(it.hasNext()){
            Statement stat = it.nextStatement();
            String subject = stat.getSubject().toString();
            String predicate = stat.getPredicate().toString();
            String object = stat.getObject().toString();
            
            if(writer!=null){
                writer.println("Subject: " + subject);
                writer.println("Predicate: " + predicate);
                writer.println("Object: " + object);
                writer.println();
            }
        }
    }
}
